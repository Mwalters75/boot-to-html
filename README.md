# Boot to HTML
Won't really boot.
## Why would I do this?
Half of Firefox OS' code was made from HTML5, CSS, and JavaScript (and some C<i>#</i>.), so I wondered: "Can I take a old, deprecated operating system, and build it... for the web?", and this is what I came up with: B2H, or Boot to HTML, but we can take out the "Boot", because it doesn't boot... At all. Because it's HTML.
## Why call it "Boot to HTML"?
Because the OS was based on HTML5, CSS, and JavaScript, except that it was called Boot to Gecko. Gecko is Firefox's engine. Think of it as Firefox's lungs. Without it,  Mozilla would shut down, because it's the main part of thier buisness.
## Is there a port of B2H to Android devices?
No, but a official version by Mozilla is [availible for download here.](https://dw.malavida.com/RHd2STRySDcwL3E5N0FwMm11QklrUlM3eXZxMzZsQXV2R1hZcldpLzRBZzh5WWFmUnA3SndPd2RIVjhBTnpVQmVBVU1WL2FVWVFEVzVESk1/DQmlZYnFheTRXTUF6ZFdYSXBsTlZicklGaGFpVjNDd3NYVkZsb00za2xySmtjMXVGc2g2eXVFVVB2YzFRV3ZNR25aNnBIendRM1h2RXlYcT/ZGcEVpZTVneG9wK0Vrdk5QT2dIdy95b3lZUitWUGM4SjZSOW5IdndTdVlHaWxFeC9yOWY0Z05iaGF5aytMRHVId1A1VHpaS0JhcEc5cDVra/3pJa2dIbnlXbFFTQnN4LzFxeEFFa1NBUzlSNnFLOFlVM2hqVjZpa3l5ck9IRG1NeFRZK1FtZ1E5SWt3YWkyZEtiWDJQcVUwaUZ1UFdCZmFV/SHpZVGVzTmN0cHZSNWduVkxoeXd1NTZlYW9McmJzb2s4bTBwZjl5SjJ3QXBtQVYxWk55cldJL1ZhZm5INjZCc2FmUnJxQ3BhUENJWUxaeXN/pN1NteTNCL1JXSEFQZlppUVgxUy9oQy8zS0xlVEV6UzBoN05UcnM3MWhuV2tEbUg1QlF4clJtcmw3cmJFWUpwclhmSnQyYTZ6SDdXN1QwU2/9QRDdMcHZuamFvMjRCY3pNUlQ3Z0hGMXFJbVkvMjFCemp6TWRWeWRxL3JjbHdiUmkvbFVTNncvOTEwSysxd2JSVFVWR3Jwa1BSV3crclNoS/3czZnJCaUR6czR0WDVUZ1B6RmlmY1NkVUlySWJ2RUhBY24zU1BxSUJIQUxjUWVuclcyRnVFY3N5MUdYbkxwaEVQUXNvWXpGK2pMM1JDbA==/a1da24445d779e38) Please note that this is pre-release software, and was never updated again since. Did you know that some design elements was taken from the app?
## What is the development status for B2H?
Due to a overwhemingly long list of applications to write from scratch, it will be in beta staus for awhile until we got the list of apps done.
## To-do
- [x] Add Homescreen
- [x] Add Camera
- [x] Add Clock
- [x] Add Map
- [ ] Add Calender
- [ ] Add Gallery
- [ ] Add E-mail
- [ ] Add Music
- [ ] Add Videos
- [ ] Add Settings
- [x] Add Ringtones
